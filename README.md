Using the website:

-By typing in the required fields, you can add a project.
 If anything that was entered is invalid, the input field will display a message.
 
-The add button will add the project to the table. From the table, you can delete an entry, edit it or sort based on a column.
 When editing, any invalid entries will be highlighted in red.

-The reset button clears the entered values in the fields.

-The save to local saves the projects in the table to local storage and overwrites previous projects.
 Append adds projects in the table to local storage without deleting any previous projects.
 Load will display all projects in local storage but will remove everything in the table.
 Clear clears the local storage.