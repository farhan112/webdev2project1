"use strict";

/**
 * Farhan Khandaker
 * Creates a project object using the inputs in the main form.
 * @returns the created project object
 */
function createProjectObject(){
    let project = {
        projectID: mainInputs[0].value,
        ownerName: mainInputs[1].value,
        title: mainInputs[2].value,
        category: mainInputs[3].value,
        status: mainInputs[4].value,
        hours: "" + parseInt(mainInputs[5].value),
        rate: "" + parseInt(mainInputs[6].value),
        description: mainInputs[7].value
    };

    return project;
}

/**
 * Farhan Khandaker
 * Adds a project to the projects array and recreates the table.
 */
function updateProjectsTable(){
    allProjects.push(createProjectObject());
    createTableFromArrayObjects(allProjects);
}

/**
 * Fu Pei Jiang
 * Deletes a row from the table and the corresponding position in the array. Recreates the table.
 * @param {number} position 
 */
function deleteTableRow(position){
    position++
    if(getByTag("tr", position).childNodes[0].childNodes[0].tagName == "INPUT"){
        alert("Please save changes before deleting.");
    }
    else{    
        let index = getIndexOfMatchingProjectFromTable(position);
        if (confirm(`Please confirm that you wish to delete project ${allProjects[index].projectID}.`)) {
            allProjects.splice(index, 1);
            createTableFromArrayObjects(allProjects);
        }
    }
}

/**
 * Farhan Khandaker
 * Takes a row from the table and finds corresponding index in allProjects.
 * @param {number} position 
 * @returns the index from allProjects
 */
function getIndexOfMatchingProjectFromTable(position){
    let row = getByTag("tr", position);
    let project2 ={
        projectID: row.childNodes[0].innerText,
        ownerName: row.childNodes[1].innerText,
        title: row.childNodes[2].innerText,
        category: row.childNodes[3].innerText,
        status: row.childNodes[4].innerText,
        hours: row.childNodes[5].innerText,
        rate: row.childNodes[6].innerText,
        description: row.childNodes[7].innerText
    };
    let index = allProjects.findIndex(
        function (project1){
            try{
                for (let key of Object.keys(project1)){
                    if(project1[key] != project2[key]){
                        throw "Not equal";
                    }
                }
                return true;
            }
            catch(error){
                return false;
            }
        }
    );
    return index;
}

/**
 * Farhan Khandaker
 * Converts table cells into inputs and saves edits made to the table and the projects array.
 * @param {number} position 
 */
function editTableRow(position){
    position++;
    let currentRow = getByTag("tr", position);
    let currentCell;
    let currentInput;
    let image;
    let index = getIndexOfMatchingProjectFromTable(position);

    for(let i = 0; i < currentRow.childNodes.length-2; i++){
        currentCell = currentRow.childNodes[i];
        
        if(i < 3 || i == 7){
            currentInput = createInputElement("text", currentCell.innerText);
        }
        else if(i == 3){
            currentInput = createSelectElement(["Practical","Theory","Fundamental Research", "Empirical"], currentCell.innerText);
        }
        else if(i == 4){
            currentInput = createSelectElement(["Completed","Ongoing","Planned"], currentCell.innerText);
        }
        else if(i == 5 || i == 6){
            currentInput = createInputElement("number", currentCell.innerText);
        }

        currentCell.childNodes[0].replaceWith(currentInput);
    }

    //Farhan Khandaker
    //Validates and saves edits made while in edit mode.
    function saveEdits(){
        let currentCell;
        let currentText;
        let image;

        try{
            for (let index = 0; index < validationRegex.length; index++) {
                if(!validateElement(validationRegex[index], currentRow.childNodes[index].childNodes[0].value)){
                    throw "Invalid Input";
                }
            }

            for(let i = 0; i <currentRow.childNodes.length-2; i++){
                currentCell = currentRow.childNodes[i].childNodes[0];
                currentText =  document.createTextNode(currentCell.value);
                currentCell.replaceWith(currentText);
            }
        
            image = currentRow.childNodes[8].childNodes[0];
            image.setAttribute("src", "../images/edit.png");
            image.setAttribute("alt", "Edit");
            currentRow.childNodes[8].addEventListener("click", editFunction);
            currentRow.childNodes[8].removeEventListener("click", saveFunction);
        
            for (let i in Object.keys(allProjects[index])){
                allProjects[index][Object.keys(allProjects[index])[i]] = currentRow.childNodes[i].innerText;
            }
        }
        catch(error){
            for (let index = 0; index < validationRegex.length; index++) {
                if(!validateElement(validationRegex[index], currentRow.childNodes[index].childNodes[0].value)){
                    currentRow.childNodes[index].childNodes[0].style.backgroundColor = "#E93F3C";
                }
            }
        }
    }
    let saveFunction = () => {saveEdits(event.currentTarget.getAttribute("value"))};

    image = currentRow.childNodes[8].childNodes[0]
    image.setAttribute("src", "../images/save.png");
    image.setAttribute("alt", "Save");
    currentRow.childNodes[8].removeEventListener("click", editFunction);
    currentRow.childNodes[8].addEventListener("click", saveFunction);
}

/**
 * Fu Pei Jiang
 * Filters the projects displayed using a substring case insensitively. Searches through all fields.
 * @param {object} query_p 
 * @param {object} search_feedback_p 
 */
 function filterProjects(query_p, search_feedback_p) {
    const resultProjects = allProjects.filter(
        function (project) {
            const query = query_p.value.toLowerCase()
            for (const value of [
                    project.projectID,
                    project.ownerName,
                    project.title,
                    project.category,
                    project.status,
                    project.hours,
                    project.rate,
                    project.description,
                ]) {
                if (value.toLowerCase().indexOf(query) > -1) {
                    return true
                }
            }
        }
    )
    createTableFromArrayObjects(resultProjects)

    search_feedback_p.style.display = "block"
    if (resultProjects.length === 1) {
        search_feedback_p.textContent = "there is 1 project for your query ..."
    } else {
        search_feedback_p.textContent = `there are ${resultProjects.length} projects for your query ...`
    }
}

/**
 * Fu Pei Jiang
 * Saves all projects to local storage.
 */
function saveAllProjectsToStorage(){
    localStorage.setItem("projects", JSON.stringify(allProjects))
}

/**
 * Fu Pei Jiang
 * Appends all projects to local storage.
 */
function appendAllProjectsToStorage(){
    const localProjectsStr = localStorage.getItem("projects")
    const localProjectsArr = localProjectsStr ? JSON.parse(localProjectsStr) : []
    const concatenated = localProjectsArr.concat(allProjects)
    localStorage.setItem("projects", JSON.stringify(concatenated))
}

/**
 * Fu Pei Jiang
 * Clears all projects to local storage.
 */
function clearAllProjectsFromStorage(){
    localStorage.removeItem("projects")
}

/**
 * Fu Pei Jiang
 * Reads all projects to local storage and recreates the table.
 */
function readAllProjectsFromStorage(){
    const localProjectsStr = localStorage.getItem("projects")
    allProjects = localProjectsStr ? JSON.parse(localProjectsStr) : []
    createTableFromArrayObjects(allProjects)
}

