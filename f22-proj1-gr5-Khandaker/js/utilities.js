"use strict";

/**
 * Farhan Khandaker
 * Gets an element by its id.
 * @param {string} id 
 * @returns the element found
 */
function getByID(id){
    return document.getElementById(id);
}

/**
 * Farhan Khandaker
 * Gets an element by its tag name and index.
 * @param {string} tag 
 * @param {number} index 
 * @returns the element found
 */
function getByTag(tag, index){
    return document.getElementsByTagName(tag)[index];
}

/**
 * Farhan Khandaker
 * Sets a form element to be required.
 * @param {object} element 
 */
function setElementRequired(element){
    element.setAttribute("required", true);
}

/**
 * Farhan Khandaker
 * Creates an element, appends to its parent and returns it as an object. Text is optional.
 * @param {object} parent 
 * @param {string} tag 
 * @param {string} text 
 * @returns the created element
 */
function createTagElement(parent, tag, text){
    let element = document.createElement(tag);
    if(Boolean(text)){
        element.innerText = text;
    }
    parent.appendChild(element);
    return element
}

/**
 * Farhan Khandaker
 * Creates an image element, appends to its parent and returns in an object.
 * @param {object} parent 
 * @param {string} source 
 * @param {string} alternative 
 * @returns the created element
 */
function createImageElement(parent, source, alternative){
    let element = createTagElement(parent, "img");
    element.setAttribute("src", source);
    element.setAttribute("alt", alternative);
    return element;
}

/**
 * Farhan Khandaker
 * Creates an input tag of proper type and default value.
 * @param {string} type 
 * @param {string} defaultValue 
 * @returns the input tag
 */
function createInputElement(type, defaultValue){
    let element = document.createElement("input");
    element.setAttribute("type", type);
    element.setAttribute("value", defaultValue);
    setElementRequired(element);
    return element;
}

/**
 * Farhan Khandaker
 * Creates a select tag with options using an array as input.
 * @param {Array} options 
 * @param {string} defaultValue 
 * @returns 
 */
function createSelectElement(options, defaultValue){
    let element = document.createElement("select");
    let option;

    for (let optionValue of options){
        option = createTagElement(element, "option", optionValue);
        option.setAttribute("value", optionValue);
    }

    element.value = defaultValue;
    return element;
}

/**
 * Farhan Khandaker
 * Displays check mark or x if inputs are valid or invalid.
 * @param {object} icon 
 * @param {object} text
 * @param {object} element 
 * @param {RegExp} pattern 
 */
function setElementFeedback(icon, text, element, pattern){
    if(validateElement(pattern, element.value)){
        icon.setAttribute("src", "../images/correct.png");
        icon.setAttribute("alt", "Correct");
        text.style.display = "none";
    }
    else{
        icon.setAttribute("src", "../images/wrong.png");
        icon.setAttribute("alt", "Wrong");
        text.style.display = "block";
    }
}

/**
 * Farhan Khandaker
 * Validates inputs using a regex pattern.
 * @param {RegExp} pattern 
 * @param {string} value 
 * @returns true or false
 */
function validateElement(pattern, value){
    return pattern.test(value);
}

/**
 * Farhan Khandaker
 * Enables or disables the add button according to if inputs are valid or invalid.
 * @param {object} add_project_p 
 */
function enableDisableButton(add_project_p){
    try {
        for(let i = 0; i < mainInputs.length; i++){
            if(!validateElement(validationRegex[i], mainInputs[i].value)){
                throw "Input is invalid";
            }
        }   
        enableButton(add_project_p);

    }
    catch (error) {
        disableButton(add_project_p);
    }
}

/**
 * Farhan Khandaker
 * Enables and button and changes the colour.
 * @param {object} button 
 */
function enableButton(button){
    button.disabled = false;
    button.style.backgroundColor = "#0561AE";
}

/**
 * Farhan Khandaker
 * Disables a button and changes the colour.
 * @param {object} button 
 */
function disableButton(button){
    button.disabled = true;
    button.style.backgroundColor = "gray";
}

/**
 * Farhan Khandaker
 * Creates the table of projects using the array of all projects.
 * @param {object} projects 
 */
function createTableFromArrayObjects(projects){
    const last_tbody = table_of_projects_e.querySelector("tbody")
        if (last_tbody) {
            table_of_projects_e.removeChild(last_tbody)
        }

    let tableBody = createTagElement(table_of_projects_e, "tbody");
    let currentRow;
    let deleteButton;
    let editButton;
    let image;

    for(let i in projects){
        currentRow = createTagElement(tableBody, "tr");
        Object.values(projects[i]).forEach(function (value){createTagElement(currentRow, "td", value);});
        
        editButton = createTagElement(currentRow, "td");
        image = createImageElement(editButton, "../images/edit.png", "Edit");
        editButton.setAttribute("class", "edit_buttons");
        editButton.setAttribute("index", i);
        editButton.addEventListener("click", editFunction);

        deleteButton = createTagElement(currentRow, "td");
        image = createImageElement(deleteButton, "../images/delete.png", "Delete");
        deleteButton.setAttribute("class", "delete_buttons");
        deleteButton.setAttribute("index", i);
        deleteButton.addEventListener("click", deleteFunction);
    }
}

//Events for delete and edit buttons. To be used for event listeners
const deleteFunction = (event) => {deleteTableRow(event.currentTarget.getAttribute("index"))};
const editFunction = (event) => {editTableRow(event.currentTarget.getAttribute("index"))};

/**
 * Fu Pei Jiang, Farhan Khandaker
 * @param {string} key 
 */
function sortByAscending(key){
    let sortedProjects = allProjects.sort(
        function (a, b){
			if(key == "hours" || key == "rate"){
				if(parseInt(a[key]) < parseInt(b[key])){
					return -1;
				}
				else{
					return 1;
				}
			}
			else{
				if(a[key].toLowerCase() < b[key].toLowerCase()){
					return -1;
				}
				else{
					return 1;
				}
			}
        }
    );
    allProjects = sortedProjects;
    createTableFromArrayObjects(allProjects);
}

/**
 * Fu Pei Jiang, Farhan Khandaker
 * @param {string} key 
 */
function sortByDescending(key){
    let sortedProjects = allProjects.sort(
        function (a, b){
			if(key == "hours" || key == "rate"){
				if(parseInt(a[key]) > parseInt(b[key])){
					return -1;
				}
				else{
					return 1;
				}
			}
			else{
				if(a[key].toLowerCase() > b[key].toLowerCase()){
					return -1;
				}
				else{
					return 1;
				}
			}
        }
    );
    allProjects = sortedProjects;
    createTableFromArrayObjects(allProjects);
}

//Functions to sort projectID by ascending and descending.
let sortProjectIDAscending = (event) => {
    sortByAscending("projectID");
    event.currentTarget.removeEventListener("click", sortProjectIDAscending);
    event.currentTarget.addEventListener("click", sortProjectIDDescending);
}
let sortProjectIDDescending = (event) => {
    sortByDescending("projectID");
    event.currentTarget.removeEventListener("click", sortProjectIDDescending);
    event.currentTarget.addEventListener("click", sortProjectIDAscending);
}

//Functions to sort owner name by ascending and descending.
let sortOwnerAscending = (event) => {
    sortByAscending("ownerName");
    event.currentTarget.removeEventListener("click", sortOwnerAscending);
    event.currentTarget.addEventListener("click", sortOwnerDescending);
}
let sortOwnerDescending = (event) => {
    sortByDescending("ownerName");
    event.currentTarget.removeEventListener("click", sortOwnerDescending);
    event.currentTarget.addEventListener("click", sortOwnerAscending);
}

//Functions to sort title by ascending and descending.
let sortTitleAscending = (event) => {
    sortByAscending("title");
    event.currentTarget.removeEventListener("click", sortTitleAscending);
    event.currentTarget.addEventListener("click", sortTitleDescending);
}
let sortTitleDescending = (event) => {
    sortByDescending("title");
    event.currentTarget.removeEventListener("click", sortTitleDescending);
    event.currentTarget.addEventListener("click", sortTitleAscending);
}

//Functions to sort category by ascending and descending.
let sortCategoryAscending = (event) => {
    sortByAscending("category");
    event.currentTarget.removeEventListener("click", sortCategoryAscending);
    event.currentTarget.addEventListener("click", sortCategoryDescending);
}
let sortCategoryDescending = (event) => {
    sortByDescending("category");
    event.currentTarget.removeEventListener("click", sortCategoryDescending);
    event.currentTarget.addEventListener("click", sortCategoryAscending);
}

//Functions to sort status by ascending and descending.
let sortStatusAscending = (event) => {
    sortByAscending("status");
    event.currentTarget.removeEventListener("click", sortStatusAscending);
    event.currentTarget.addEventListener("click", sortStatusDescending);
}
let sortStatusDescending = (event) => {
    sortByDescending("status");
    event.currentTarget.removeEventListener("click", sortStatusDescending);
    event.currentTarget.addEventListener("click", sortStatusAscending);
}

//Functions to sort hours by ascending and descending.
let sortHoursAscending = (event) => {
    sortByAscending("hours");
    event.currentTarget.removeEventListener("click", sortHoursAscending);
    event.currentTarget.addEventListener("click", sortHoursDescending);
}
let sortHoursDescending = (event) => {
    sortByDescending("hours");
    event.currentTarget.removeEventListener("click", sortHoursDescending);
    event.currentTarget.addEventListener("click", sortHoursAscending);
}

//Functions to sort rate by ascending and descending.
let sortRateAscending = (event) => {
    sortByAscending("rate");
    event.currentTarget.removeEventListener("click", sortRateAscending);
    event.currentTarget.addEventListener("click", sortRateDescending);
}
let sortRateDescending = (event) => {
    sortByDescending("rate");
    event.currentTarget.removeEventListener("click", sortRateDescending);
    event.currentTarget.addEventListener("click", sortRateAscending);
}

//Functions to sort description by ascending and descending.
let sortDescriptionAscending = (event) => {
    sortByAscending("description");
    event.currentTarget.removeEventListener("click", sortDescriptionAscending);
    event.currentTarget.addEventListener("click", sortDescriptionDescending);
}
let sortDescriptionDescending = (event) => {
    sortByDescending("description");
    event.currentTarget.removeEventListener("click", sortDescriptionDescending);
    event.currentTarget.addEventListener("click", sortDescriptionAscending);
}