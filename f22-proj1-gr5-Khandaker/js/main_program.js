"use strict";

let allProjects = [];

const validationRegex = [
    /^[a-z]([a-z]|[0-9]|\$|\-|\_){2,9}$/i,//Project ID
    /^[a-z]([a-z]|[0-9]|\-){2,9}$/i,//Owner Name
    /^[a-z]{3,25}$/i,//Title
    /[a-z]/i,//Category
    /[a-z]/i,//Status
    /^[0-9]{1,3}$/,//Hours
    /^[0-9]{1,3}$/,//Rate
    /^.{3,65}$///Short Description
];

let table_of_projects_e;

let mainInputs;

window.addEventListener("DOMContentLoaded", initialise);

/**
 * Farhan Khandaker, Fu Pei Jiang
 * Initialises the javascript code.
 */
function initialise(){

    table_of_projects_e = getByID("table_of_projects")

    mainInputs = [
        getByID("project_id"),
        getByID("owner_name"),
        getByID("title"),
        getByID("category"),
        getByID("status"),
        getByID("hours"),
        getByID("rate"),
        getByID("description")
    ];

    const feedbackImages = [
        getByID("project_id_feedback"),
        getByID("owner_name_feedback"),
        getByID("title_feedback"),
        getByID("category_feedback"),
        getByID("status_feedback"),
        getByID("hours_feedback"),
        getByID("rate_feedback"),
        getByID("description_feedback")
    ];

    const feedbackText = [
        getByID("project_id_text"),
        getByID("owner_name_text"),
        getByID("title_text"),
        getByID("category_text"),
        getByID("status_text"),
        getByID("hours_text"),
        getByID("rate_text"),
        getByID("description_text")
    ];

    //Initialising feedback when an input is made.
    for (let i = 0; i < mainInputs.length; i++) {
        mainInputs[i].addEventListener("blur", function (event){setElementFeedback(feedbackImages[i], feedbackText[i], event.currentTarget, validationRegex[i])});
        mainInputs[i].addEventListener("input", function (event){setElementFeedback(feedbackImages[i], feedbackText[i], event.currentTarget, validationRegex[i])});
    }

    //Initialsing the button to add projects.
    const add_project_e = getByID("add_project")
    add_project_e.addEventListener("click", updateProjectsTable);
    disableButton(add_project_e);

    //Initialising the button to reset inputs.
    const resetButton = getByID("reset_inputs");
    resetButton.addEventListener("click", function (){
        disableButton(add_project_e);
    });
    mainInputs.forEach(function (input){input.addEventListener("input", function (){enableDisableButton(add_project_e)})});

    //Initialising the query.
    const query_e = getByID("query")
    const search_feedback_e = getByID("search_feedback")
    query_e.addEventListener("input", function (){filterProjects(query_e, search_feedback_e)});

    //Initialising save to local.
    const save_local_e = getByID("save_local")
    save_local_e.addEventListener("click", function() {
        saveAllProjectsToStorage()
        search_feedback_e.style.display = "block"
        if (allProjects.length === 1) {
            search_feedback_e.textContent = "There is now 1 project in local storage."
        } else {
            search_feedback_e.textContent = `There are now ${allProjects.length} projects in local storage.`
        }
    })

    //Initialising append to local.
    const append_local_e = getByID("append_local")
    append_local_e.addEventListener("click", function() {
        appendAllProjectsToStorage()
        search_feedback_e.style.display = "block"
        if (allProjects.length === 1) {
            search_feedback_e.textContent = "Added 1 project to local storage."
        } else {
            search_feedback_e.textContent = `Added ${allProjects.length} projects to local storage.`
        }
    })

    //Initialising clear local storage.
    const clear_local_e = getByID("clear_local")
    clear_local_e.addEventListener("click", function() {
        clearAllProjectsFromStorage()
        search_feedback_e.style.display = "block"
        search_feedback_e.textContent = "There are now no projects in local storage."
    })

    //Initialising load from local storage.
    const load_local_e = getByID("load_local")
    load_local_e.addEventListener("click", function() {
        readAllProjectsFromStorage()
        search_feedback_e.style.display = "block"
        if (allProjects.length === 1) {
            search_feedback_e.textContent = "Loaded a project from local storage."
        } else {
            search_feedback_e.textContent = `Loaded ${allProjects.length} projects from local storage.`
        }
    })

    //Initialising sorting buttons
    const thead_project_id_e = getByID("thead_project_id");
    thead_project_id_e.addEventListener("click", sortProjectIDAscending);

    const thead_owner_name_e = getByID("thead_owner_name");
    thead_owner_name_e.addEventListener("click", sortOwnerAscending);

    const thead_title_e = getByID("thead_title");
    thead_title_e.addEventListener("click", sortTitleAscending);

    const thead_category_e = getByID("thead_category");
    thead_category_e.addEventListener("click", sortCategoryAscending);

    const thead_status_e = getByID("thead_status");
    thead_status_e.addEventListener("click", sortStatusAscending);

    const thead_hours_e = getByID("thead_hours");
    thead_hours_e.addEventListener("click", sortHoursAscending);

    const thead_rate_e = getByID("thead_rate");
    thead_rate_e.addEventListener("click", sortRateAscending);

    const thead_description_e = getByID("thead_description");
    thead_description_e.addEventListener("click", sortDescriptionAscending);

}